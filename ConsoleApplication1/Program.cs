﻿using System;
using System.Configuration;
using System.Collections.Specialized;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Diagnostics;
using System.IO;



namespace ConsoleApplication1
{
    
    
    class Program
    {
        

        static void Main(string[] args)
        {
            DateTime Archivedate = Properties.Settings.Default.ArchiveDate;
            String Sourcepath = Properties.Settings.Default.SourcePath;
            String Targetpath = Properties.Settings.Default.TargetPath;
             //string configvalue3 = System.Configuration.ConfigurationManager.AppSettings["Source"];

            var folders = Directory.EnumerateDirectories(Sourcepath);
            foreach(var folder in folders)
            {
                var files = Directory.EnumerateFiles(folder);
                foreach(var file in files)
                {
                    if(DateTime.Compare(File.GetLastWriteTime(file),new DateTime(Archivedate))<0)
                    {
                        File.Copy(Sourcepath,Targetpath);
                       // Trace.WriteLine(file + ": " + File.GetLastWriteTime(file).ToString());
                    }
                }
                
            }
        }
    }
}
